from django.test import SimpleTestCase
from budget.forms import ExpenseForm


class TestForm(SimpleTestCase):


#this is test just incase if data is received
    def test_expense_valid_form_data(self):
        form=ExpenseForm(
            data={
                'title':'expense1',
                'amount':'1000',
                'category':'development'
            }
        )
        
        self.assertTrue(form.is_valid())

#this is test just in case if data is not received
   
    def test_form_with_no_data(self):
        form=ExpenseForm(
            data={}
        )
    
        self.assertFalse(form.is_valid())
        self.assertEquals(len(form.errors),3)