# from django.test import TestCase,Client
# from django.urls import reverse
# from budget.models import Project,Category,Expense
# import json

# class TestViews(TestCase):

# #this method wok like common to all test that are called
#     def setUp(self):
#         self.client=Client()
#         self.list_urls=reverse('list')
#         self.detail_url=reverse('detail',args=['project1'])
#         self.project1=Project.objects.create(name='project1',budget=10000)

# #thi
#     def test_project_client_get(self):
#         response=self.client.get(self.list_urls)
#         self.assertEquals(response.status_code,200)

#         self.assertTemplateUsed(response,'budget/project-list.html')


 
# #this is for deatil view get method

#     def test_project_client_get1(self):
#         respone=self.client.get(self.detail_url)
#         self.assertEquals(respone.status_code,200)

#         self.assertTemplateUsed(respone,'budget/project-detail.html')

# #this is for deatil view post method...

#     def test_project_post_detail(self):
#         Category.objects.create(project=self.project1,
#                                 name='developement')
        
#         response=self.client.post(self.detail_url,{
#               'title':'expense1',
#               'amount':1000,
#               'category':'developement'

#         })

#         self.assertEqual(response.status_code,302)
#         self.assertEqual(self.project1.expenses.first().title,'expense1')


#     #this is for detail view delete method

#     def test_project_post_delete(self):
#         category1= Category.objects.create(project=self.project1,
#                                 name='developement')

#         Expense.objects.create(project=self.project1,
#                                title='expense1',
#                                amount=1000,
#                                category=category1)
        
#         respone=self.client.delete(self.detail_url,json.dumps({
#             'id':1
#         }))

#         self.assertEquals(respone.status_code,204)
#         self.assertEquals(self.project1.expenses.count(),0)

#     #this is for detail delete method with no id...

#     def test_project_delete_id_no(self):
#         category1= Category.objects.create(project=self.project1,
#                                 name='developement')

#         Expense.objects.create(project=self.project1,
#                                title='expense1',
#                                amount=1000,
#                                category=category1)
        
#         response=self.client.delete(self.detail_url)

#         self.assertEquals(response.status_code,404)

#         self.assertEquals(self.project1.expenses.count(),1)

# #this view is for add post test for project

#     def test_for_post(self):
#         url=reverse('add')
#         response=self.client.post(url,{
#             'name':'project2',
#             'budget':10000,
#             'categoriesString':'design,developement'
#         })
#         project2=Project.objects.get(id=2)
#         self.assertEquals(project2.name,'project2')
#         first_category=Category.objects.get(id=1)
#         self.assertEquals(first_category.project,project2)
#         self.assertEquals(first_category.name,'design')
#         second_category=Category.objects.get(id=2)
#         self.assertEquals(second_category.project,project2)
#         self.assertEquals(second_category.name,'developement')

        