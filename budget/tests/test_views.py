from django.test import TestCase,Client
from django.urls import reverse
from budget.models import Project,Category,Expense
import json


class TestViews(TestCase):

    def setUp(self):
        print('mysetup')
        self.client=Client()
        self.list_urls=reverse('list')
        self.detail_ulr=reverse('detail',args=['project1'])
        self.project1=Project.objects.create(name='project1',budget=10000)

    def test_get_list(self):
        response=self.client.get(self.list_urls)
        print('test written by mukesh')
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,'budget/project-list.html')